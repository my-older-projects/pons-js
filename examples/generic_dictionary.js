'use strict';
const print = obj => console.dir(obj, {depth: null});
const PONS = require('../');

let Generic_Dictionary = new PONS(); // not implemented yet

Generic_Dictionary.lookup('türkiye', (err, answer) => {
	print(answer);
});

Generic_Dictionary.lookup('turkey', (err, answer) => {
	print(answer);
});

Generic_Dictionary.lookup('holiday', (err, answer) => {
	print(answer);
});

Generic_Dictionary.lookup('feier', (err, answer) => {
	print(answer);
});

Generic_Dictionary.lookup('amor', (err, answer) => {
	print(answer);
});
