'use strict';
const PONS = require('../');

let Deutsch_English_Wörterbuch = new PONS({lang: ['de', 'en'], flags: {noExample: true}});

Deutsch_English_Wörterbuch.traverse(process.argv.slice(2)[0], (err, answer) => {
	console.dir(answer, {depth: null});
});
