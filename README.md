# PONS.js

PONS Wörterbuch for Node.js

### Installing

```
npm install pons.js
```

### Usage

```
const PONS = require('pons.js');

let Deutsch_English_Wörterbuch = new PONS({lang: ['de', 'en']});

Deutsch_English_Wörterbuch.translate('brennen', jsonTreeData => {
	console.dir(jsonTreeData, {depth: null});
});
```

## Example Data

Rucksack (English - German) [tree](examples/json/rucksack.json), [list](examples/json/rucksackList.json)

Evolution (English - German) [tree](examples/json/evolution.json), [list](examples/json/evolutionList.json)

brennen (English - German) [tree](examples/json/brennen.json), [list](examples/json/brennenList.json)

Kağıt (Turkish - German) [tree](examples/json/kağıt.json)

Amour (French - Chinese) [tree](examples/json/amour.json)

## TODO

* Finish nullitranslate method and ~~fix exactWord~~ [FIXED]. Do not use them currenty.

## Built With

* Love
* Love
* More love

## Contributing

You're free to contribute of course!

## Authors

* **Ömer Yağmurlu** - *Creator* - [omeryagmurlu](https://github.com/omeryagmurlu)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the ISC License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* ~~Currently I am using jsdom with jquery to parse the response body, if you know a better, faster and not harder way to do it, please inform me through issues.~~ DONE. Cheerio
* This API ~~will be~~ is used in [my first Telegram Bot](http://storebot.me/bot/ponsobot), check it out!
