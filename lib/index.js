'use strict';
const _pons = require('./pons.js');
const noop = () => {};

module.exports = class Pons {
	constructor(opts) {
		opts && Object.getOwnPropertyNames(opts).forEach(val => this[val] = opts[val]);

		this.setLanguage(this.lang);
		this.setFlags(this.flags);
	}

	translate(_word, cb) {
		cb = cb || noop;

		let pons = new _pons(this.flags);

		if ((!this.from) && (!this.to)) {
			pons.nullitranslate(_word, cb);
			return this;
		}

		if ((!this.from) || (!this.to)) {
			pons.unitranslate(_word, this.from || this.to, cb);
			return this;
		}

		pons.translate(_word, this.from, this.to, cb);
		return this;
	}

	traverse(_word, cb) {
		return this.translate(_word, cb);
	}

	lookup(_word, cb) {
		return this.translateTree(_word, cb);
	}

	translateTree(_word, cb) {
		cb = cb || noop;
		return this.translate(_word, (err, translation) => {
			if (err) {
				return cb(err);
			}
			let arr = [];
			translation.forEach(element0 => {
				let lang = element0.words;
				lang.forEach(element1 => {
					let _0 = element1.words;
					_0.forEach(element2 => {
						let _1 = element2.words;
						_1.forEach(element3 => {
							arr.push({});
							let arrEnd = arr[arr.length - 1];
							for (let key in element0) {
								if (key !== 'words') {
									arrEnd[key] = element0[key];
								}
							}
							for (let key in element1) {
								if (key !== 'words') {
									arrEnd[key] = element1[key];
								}
							}
							for (let key in element2) {
								if (key !== 'words') {
									arrEnd[key] = element2[key];
								}
							}
							for (let key in element3) {
								if (lang) {} // tricking eslint
								arrEnd[key] = element3[key];
							}
						});
					});
				});
			});
			return cb(null, arr);
		});
	}

	setLanguage(arr) {
		if (!arr) {
			return this.setGlobal();
		}

		if (typeof arr === 'string') {
			return this.setLanguage([arr, null]);
		}

		this.to = arr[0];
		this.from = arr[1];

		return this;
	}

	setGlobal() {
		return this.setLanguage([null, null]);
	}

	setFlags(obj) {
		if (!obj) {
			this.flags = {};
			return;
		}

		for (let flag in obj) {
			this.flags[flag] = obj[flag];
		}

		return this;
	}
};

// curl 'http://tr.pons.com/%C3%A7eviri?q=tritt&l=detr&in=&lf=de&cid=&srt=null'
// -H 'pragma: no-cache'
// -H 'dnt: 1'
// -H 'accept-language: en-US;q=0.6,en;q=0.4'
// -H 'upgrade-insecure-requests: 1'
// -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
// -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
// -H 'cache-control: no-cache'
// --compressed
